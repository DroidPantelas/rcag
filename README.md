# Random Corner Art Generator

Main file: rcag.html

Designed for a GCSE art project
Created By Pandelis Zembashis - CY131 - 5082

===============================================

The 21st century has brought with it the most radical and exponential changes in technology ever in the history of man. All this technology provides boundless opportunities to express our imagination. So why limit ourselves to paper? 

Computers should not be overlooked as a tool to create art. So using the theme of corners and polygons I wrote my own custom computer program that generates repetitive geometrical art. Using the different options I have added to the algorithm it is possible to create infinitely unique combinations of shapes.

===============================================

Libraries Used:

Jquery

Spectrum - Color Picker >> http://bgrins.github.com/spectrum/ 

Raphael - Canvas SVG generator >> http://raphaeljs.com/
